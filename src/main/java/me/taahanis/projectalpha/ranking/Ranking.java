/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.ranking;

import java.util.Arrays;
import java.util.List;
import me.taahanis.projectalpha.Alpha;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author moh_a
 */

public class Ranking {
    
    public static Alpha plugin = Alpha.getPlugin(Alpha.class);
    public static final List<String> ADMINS = Arrays.asList("administrator", "admin", "Admin");
    public enum Rank {
    
        DEFAULT("Default", ChatColor.DARK_GRAY + "" + ChatColor.BOLD + "DEFAULT", 0),
        HELPER("Helper", ChatColor.YELLOW + "" + ChatColor.BOLD + "HELPER", 1),
        MOD("Moderator", ChatColor.DARK_AQUA + "" + ChatColor.BOLD + "MOD", 2),
        ADMIN("Administrator", ChatColor.RED + "" + ChatColor.BOLD + "ADMIN", 3),
        DEVELOPER("Developer", ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "DEVELOPER", 4),
        OWNER("Owner", ChatColor.BLUE + "" + ChatColor.BOLD + "OWNER", 5),
        CONSOLE("Console", ChatColor.DARK_PURPLE + "" + ChatColor.BOLD + "CONSOLE", 6);
        
    
    private final String name;
    private final String prefix;
    private final int level;

    private Rank(String name, String prefix, int level)
    {
        this.name = name;
        this.prefix = prefix;
        this.level = level;

    }
    
    public int getLevel()
    {
        return level;
    }
    
    public boolean isAbout(Rank rank) {
            return level >= rank.getLevel();
        }
    public String getName()
    {
        return name;
    }
    public String getPrefix()
    {
        return prefix;
    }
    
    
    public static boolean isStaff(CommandSender sender)
    {
        Player player = (Player) sender;
        return plugin.cm.getStaff().contains(player.getUniqueId().toString());
    }
    
    public static boolean Helper(CommandSender sender)
    {
        Player player = (Player) sender;
        return isStaff(player) && plugin.cm.getStaff().getString(player.getUniqueId().toString() + ".rank").equalsIgnoreCase("helper");
    }
    public static boolean Mod(CommandSender sender)
    {
        Player player = (Player) sender;
        return isStaff(player) && plugin.cm.getStaff().getString(player.getUniqueId().toString() + ".rank").equalsIgnoreCase("moderator");
    }
    public static boolean Admin(CommandSender sender)
    {
        Player player = (Player) sender;
        return isStaff(player) && plugin.cm.getStaff().getString(player.getUniqueId().toString() + ".rank").equalsIgnoreCase("administrator");
    }
    public static boolean Developer(CommandSender sender)
    {
        Player player = (Player) sender;
        return isStaff(player) && plugin.cm.getStaff().getString(player.getUniqueId().toString() + ".rank").equalsIgnoreCase("developer");
    }
    public static boolean Owner(CommandSender sender)
    {
        Player player = (Player) sender;
        return isStaff(player) && plugin.cm.getStaff().getString(player.getUniqueId().toString() + ".rank").equalsIgnoreCase("owner");
    }
    
    public static Rank findRank(String string)
    {
        try
        {
            return Rank.valueOf(string.toUpperCase());
        }
        catch (Exception ignored)
        {
        }

        return Rank.DEFAULT;
    }
    
    public static Rank getRank(CommandSender sender)
    {
        if (!(sender instanceof Player))
        {
            return Rank.CONSOLE;
        }
        final Player player = (Player) sender;
        if (isStaff(player))
        {
            if (Helper(player))
        {
            return Rank.HELPER;
        }
        if (Mod(player))
        {
            return Rank.MOD;
        }
        if (Admin(player))
        {
            return Rank.ADMIN;
        }
        if (Developer(player))
        {
            return Rank.DEVELOPER;
        }
        if (Owner(player))
        {
            return Rank.OWNER;
        }
        
        }
        
        return Rank.DEFAULT;
    }
    
    
    public static String getTag(CommandSender sender)
    {
        final Player player = (Player) sender;
        
        if (Helper(player))
        {
            return Rank.HELPER.getPrefix();
        }
        if (Mod(player))
        {
            return Rank.MOD.getPrefix();
        }
        if (Admin(player))
        {
            return Rank.ADMIN.getPrefix();
        }
        if (Developer(player))
        {
            return Rank.DEVELOPER.getPrefix();
        }
        if (Owner(player))
        {
            return Rank.OWNER.getPrefix();
        }
        
        return Rank.DEFAULT.getPrefix();
    }
    
    
    
    
    
}
}
