/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.command;

import me.taahanis.projectalpha.ranking.Ranking;
import me.taahanis.projectalpha.ranking.Ranking.Rank;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author moh_a
 */
public class Command_tpo implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
        {
            sender.sendMessage("This is a ingame command.");
            return true;
        }
        if (args.length == 0)
        {
            sender.sendMessage(ChatColor.RED + "Correct usage: /tpo <player>");
            return true;
        }
        Player player = (Player) sender;
        if (!Rank.getRank(player).isAbout(Rank.MOD))
        {
            sender.sendMessage(ChatColor.RED + "You must be atleast a Moderator (MOD) or higher to use this command.");
            return true;
        }
        Player target = Bukkit.getServer().getPlayer(args[0]);
        if (target == null)
        {
            player.sendMessage(ChatColor.GOLD + "Player not found.");
        }
        player.sendMessage(ChatColor.AQUA + "You have teleported to " + target.getName());
        target.sendMessage(ChatColor.AQUA + player.getName() + " has teleported to you.");
        player.teleport(target.getLocation());
        return true;
    }
    
}
