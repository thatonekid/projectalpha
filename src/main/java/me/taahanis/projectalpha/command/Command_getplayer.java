package me.taahanis.projectalpha.command;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import me.taahanis.projectalpha.Alpha;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Command_getplayer implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (args.length == 0 || args.length > 1)
        {
            sender.sendMessage(ChatColor.RED + "Correct usage: /getplayer <playername>");
            return true;
        }
        if (!(sender instanceof Player)) {
            sender.sendMessage(ChatColor.RED + "Ingame command, sorry.");
            return true;
        }
        Player player = (Player) sender;
        Player target = Bukkit.getServer().getPlayer(args[0]);
        if (target == null) {
            player.sendMessage("Player not found.");
            return true;
        }
        DBObject r = new BasicDBObject("uuid", target.getUniqueId());
        DBObject found = Alpha.plugin.connection.getPlayersConllection().findOne(r);
        if (found == null) {
            player.sendMessage(ChatColor.RED + "Player not found.");
            return true;
        }
        String name = (String) found.get("name");
        player.sendMessage(ChatColor.RED + "Player Name - " + name);
        player.sendMessage(ChatColor.GOLD + "Player UUID - " + target.getUniqueId().toString());
        player.sendMessage(ChatColor.AQUA + "Player Race - " + Alpha.plugin.rm.getRace(target));
        return true;
    }
}
