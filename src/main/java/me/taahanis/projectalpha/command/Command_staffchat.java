/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.command;

import java.util.ArrayList;
import me.taahanis.projectalpha.Alpha;
import me.taahanis.projectalpha.StaffChat;
import me.taahanis.projectalpha.ranking.Ranking;
import me.taahanis.projectalpha.ranking.Ranking.Rank;
import net.md_5.bungee.api.ChatColor;
import org.apache.commons.lang3.StringUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author moh_a
 */
public class Command_staffchat implements CommandExecutor {

    
    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (!(sender instanceof Player))
        {
            sender.sendMessage("This is a ingame command.");
            return true;
        }
        Player player = (Player) sender;
        if (!Rank.isStaff(player))
        {
            player.sendMessage(ChatColor.RED + "You are not a staff member.");
            return true;
        }
        if (args.length == 0)
        {
            if (!Alpha.plugin.cm.getStaff().getBoolean(player.getUniqueId().toString() + ".adminchat"))
            {
                player.sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + "Toggling on staff chat.");
                Alpha.plugin.cm.getStaff().set(player.getUniqueId().toString() + ".adminchat", true);
                return true;
                
            }
            if (Alpha.plugin.cm.getStaff().getBoolean(player.getUniqueId().toString() + ".adminchat"))
            {
                player.sendMessage(ChatColor.GRAY + "" + ChatColor.ITALIC + "Toggling off staff chat.");
                Alpha.plugin.cm.getStaff().set(player.getUniqueId().toString() + ".adminchat", false);
                return true;
            }
            return true;
        } else {
            StaffChat.StaffChatMsg(sender, StringUtils.join(args, " "));
            return true;
        } 
        
        
    }
    
}
