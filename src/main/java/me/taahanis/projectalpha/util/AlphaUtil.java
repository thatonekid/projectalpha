/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.util;

import net.md_5.bungee.api.ChatColor;
import org.bukkit.command.CommandSender;

/**
 *
 * @author moh_a
 */
public class AlphaUtil {
    
    public static void sendMsg(CommandSender sender, String message, ChatColor color)
    {
        sender.sendMessage(color + message);
    }
    
    public static void sendMsg(CommandSender sender, String message)
    {
        AlphaUtil.sendMsg(sender, message, ChatColor.GRAY);
    }
    
}
