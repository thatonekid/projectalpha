package me.taahanis.projectalpha.mysql;

import com.mongodb.*;
import me.taahanis.projectalpha.Alpha;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

public class Connection {
    private Alpha plugin;
    public Connection(Alpha instance)
    {
        plugin = instance;
    }

    public DBCollection players;
    public DB projectalpha;
    public MongoClient client;

    public boolean connect()
    {
        //Connect to the specified ip and port
        //Default is localhost, 27017
        try {
            ServerAddress addr = new ServerAddress("insertaddress", 1234);
            List<MongoCredential> credentials = new ArrayList<>();
            credentials.add(MongoCredential.createCredential("username", "database", "password".toCharArray()));
            client = new MongoClient(addr, credentials);

        } catch (UnknownHostException e) {
            //When you end up here, the server the db is running on could not be found!
            System.out.println("Could not connect to database!");
            e.printStackTrace();
            return false;
        }
        //Get the database called "mcserver"
        //If it does not exist it will be created automatically
        //once you save something in it
        projectalpha = client.getDB("projectalpha");
        //Get the collection called "players" in the database "mcserver"
        //Equivalent to the table in MySQL, you can store objects in here
        players = projectalpha.getCollection("players");
        return true;
    }

    public DBCollection getPlayersConllection()
    {
        return players;
    }

    public DB getDatabase()
    {
        return projectalpha;
    }

    public MongoClient getClient()
    {
        return client;
    }
}
