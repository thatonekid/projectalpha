package me.taahanis.projectalpha.mysql;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import me.taahanis.projectalpha.Alpha;
import me.taahanis.projectalpha.util.AlphaLog;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import java.sql.SQLException;
import java.util.UUID;

public class MySQLGetterSetter implements Listener {

    Alpha plugin = Alpha.getPlugin(Alpha.class);

    @EventHandler
    public void onJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        playerExists(player.getUniqueId(), player.getName(), player);
    }

    public void playerExists(UUID uuid, String name, Player player) {
        try {
            DBObject r = new BasicDBObject("uuid", uuid);

            DBObject found = plugin.connection.getPlayersConllection().findOne(r);
            if (found != null)
            {
                return;
            }
            createPlayer(player.getUniqueId(), player);

            AlphaLog.info("Checked MongoDB");
        } catch (Exception e)
        {
            AlphaLog.info("Could not correctly check if player exists in MongoDB!!!");
            e.printStackTrace();
        }
    }

    public void createPlayer(final UUID uuid, Player player) {
        try {
                DBObject obj = new BasicDBObject("uuid", uuid);
                obj.put("name", player.getName());
                plugin.connection.getPlayersConllection().insert(obj);
                AlphaLog.info("Created player");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


