/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha;

import me.taahanis.projectalpha.ranking.Ranking;
import me.taahanis.projectalpha.ranking.Ranking.Rank;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author moh_a
 */
public class StaffChat {
    
    public static void StaffChatMsg(CommandSender sender, String message)
    {
        String name = sender.getName();
        message = ChatColor.translateAlternateColorCodes('&', message);
        
        for (Player players : Bukkit.getOnlinePlayers())
        {
            if (Rank.isStaff(players))
            {
                players.sendMessage(ChatColor.DARK_RED + "[" + ChatColor.GOLD + "Staff Chat" + ChatColor.DARK_RED + "] " + Rank.getRank(sender).getPrefix() + " " + ChatColor.GREEN + name + ChatColor.GRAY + ": " + message);
            }
        }
        
    }
    
}
