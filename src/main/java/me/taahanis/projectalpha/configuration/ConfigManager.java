/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.configuration;

import java.io.File;
import me.taahanis.projectalpha.Alpha;
import me.taahanis.projectalpha.util.AlphaLog;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

/**
 *
 * @author moh_a
 */
public class ConfigManager {
    
    public ConfigManager()
    {
        
    }
    
    public Alpha plugin = Alpha.getPlugin(Alpha.class);
    
    FileConfiguration players;
    File playersFile;
    
    FileConfiguration staff;
    File staffFile;
    
    FileConfiguration race;
    File raceFile;
    
    
    
    public void setup()
    {
        if (!plugin.getDataFolder().exists())
        {
            plugin.getDataFolder().mkdir();
        }
        
        staffFile = new File(plugin.getDataFolder(), "staff.yml");
        playersFile = new File(plugin.getDataFolder(), "players.yml");
        raceFile = new File(plugin.getDataFolder(), "races.yml");
        race = YamlConfiguration.loadConfiguration(raceFile);
        players = YamlConfiguration.loadConfiguration(playersFile);
        staff = YamlConfiguration.loadConfiguration(staffFile);
        
        try {
            race.save(raceFile);
            staff.save(staffFile);
            players.save(playersFile);
            
            AlphaLog.info("Configs saved, setup completed.");
        } catch (Exception e)
        {
            AlphaLog.info("Configs did not load in correctly.");
        }
        
    }
    
    public FileConfiguration getPlayers()
    {
        return players;
    }
    
    public FileConfiguration getStaff()
    {
        return staff;
    }
    
    public FileConfiguration getRaces()
    {
        return race;
    }
    
    public void reload()
    {
        setup();
        try {
            race.save(raceFile);
            staff.save(staffFile);
            players.save(playersFile);
            race.load(raceFile);
            players.load(playersFile);
            staff.load(staffFile);
        } catch (Exception e)
        {
            AlphaLog.info("Unable to reload.");
        }
    }
    
    public void save()
    {
     try {
         race.save(raceFile);
         staff.save(staffFile);
         players.save(playersFile);
         AlphaLog.info("Configs saved! Woot woot.");
     } catch (Exception e)
     {
         AlphaLog.info("Configs didn't save, try again.");
     }
    }
    
}
