/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ListenerOptions;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedGameProfile;
import me.taahanis.projectalpha.command.*;
import me.taahanis.projectalpha.configuration.ConfigManager;
import me.taahanis.projectalpha.listener.PlayerListener;
import me.taahanis.projectalpha.listener.RaceListener;
import me.taahanis.projectalpha.listener.ScoreboardListener;
import me.taahanis.projectalpha.mysql.Connection;
import me.taahanis.projectalpha.mysql.MySQLGetterSetter;
import me.taahanis.projectalpha.ranking.Staff;
import me.taahanis.projectalpha.util.AlphaLog;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author moh_aukk
 */
public class Alpha extends JavaPlugin {
    
    public static Alpha plugin;

    
    public ConfigManager cm;
    public Staff staff = new Staff(this);
    public Functions func = new Functions(this);

    public RaceManager rm = new RaceManager(this);

    public Connection connection = new Connection(this);

    
    @Override
    public void onEnable()
    {
        plugin = this;
        cm = new ConfigManager();
        
        
        
        cm.setup();

        try {
            connection.connect();
            AlphaLog.info("Connected to MongoDB!!");
        } catch (Exception e)
        {
            AlphaLog.info("Unable to connect to host - MongoDB");
        }
        
        registerCommands();
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new PlayerListener(), this);
        pm.registerEvents(new RaceListener(), this);
        pm.registerEvents(new ScoreboardListener(), this);
        pm.registerEvents(new MySQLGetterSetter(), this);
        
        getConfig().options().copyDefaults(true);
        saveConfig();

        setPlayerCountMsg();

        //testing bitbucket webhook for discord
    }



    public void registerCommands()
    {
        getCommand("rank").setExecutor(new Command_rank());
        getCommand("admin").setExecutor(new Command_admin());
        getCommand("list").setExecutor(new Command_list());
        getCommand("clear").setExecutor(new Command_clear());
        getCommand("staffchat").setExecutor(new Command_staffchat());
        getCommand("tpo").setExecutor(new Command_tpo());
        getCommand("betaitems").setExecutor(new Command_betaitems());
        getCommand("getplayer").setExecutor(new Command_getplayer());
    }


    public void setPlayerCountMsg()
    {
        final List<String> names2 = new ArrayList<String>();
        final List<WrappedGameProfile> names = new ArrayList<WrappedGameProfile>();
        names.add(new WrappedGameProfile("1", ChatColor.RED + "" + ChatColor.BOLD + "ProjectAlpha " + ChatColor.GRAY + "" + ChatColor.BOLD + "»" + ChatColor.YELLOW + " 1.12.2"));

        names.add(new WrappedGameProfile("3", ChatColor.AQUA + "► Custom RPG!"));
        names.add(new WrappedGameProfile("4", ChatColor.BLUE + "► No website yet!"));
        names.add(new WrappedGameProfile("4", ChatColor.GOLD + "► https://discord.gg/WBubSNN"));
        //If you want to add more message, copy 'names.add(new WrappedGameProfile("number", "ur message"));'
        //Make sure that 'number' goes in order. Instance:
        //names.add(new WrappedGameProfile("1", ChatColor.LIGHT_PURPLE + "This is message 1!"));
        //names.add(new WrappedGameProfile("2", ChatColor.GREEN + "This is message 2!"));
        //names.add(new WrappedGameProfile("3", ChatColor.GREEN + "This is message 3!"));
        //names.add(new WrappedGameProfile("4", ChatColor.GREEN + "This is message 4!"));
        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL,
                Arrays.asList(PacketType.Status.Server.OUT_SERVER_INFO), ListenerOptions.ASYNC) {
            @Override
            public void onPacketSending(PacketEvent event) {
                event.getPacket().getServerPings().read(0).setPlayers(names);
                event.getPacket().getServerPings().read(0).setVersionProtocol(3);
                event.getPacket().getServerPings().read(0).setVersionName(ChatColor.GRAY + "Online → " + ChatColor.BLUE + Bukkit.getServer().getOnlinePlayers().size() + ChatColor.WHITE +  "/" + ChatColor.RED + Bukkit.getServer().getMaxPlayers());
                event.getPacket().getServerPings().read(0).setMotD(
                        ChatColor.RED + "" + ChatColor.BOLD + "ProjectAlpha " + ChatColor.GRAY + "" + ChatColor.BOLD + "»" +
                ChatColor.YELLOW + " 1.12.2" +
                ChatColor.GREEN + "" + ChatColor.BOLD + "\nBETA RELEASE");
            }
        });
    }


    @Override
    public void onDisable()
    {
        connection.getClient().close();

        saveConfig();
    }
    
}
