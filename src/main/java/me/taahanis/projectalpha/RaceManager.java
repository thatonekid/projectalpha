package me.taahanis.projectalpha;

import org.bukkit.entity.Player;

public class RaceManager {

    public Alpha plugin;
    public RaceManager(Alpha instance)
    {
        plugin = instance;
    }

    public boolean hasRace(Player player)
    {
        String pSection = Alpha.plugin.cm.getRaces().getString(player.getUniqueId().toString());
        if (pSection == null)
        {
            return false;
        }
        return true;
    }

    public boolean isElf(Player player)
    {
        return Alpha.plugin.cm.getRaces().getString(player.getUniqueId().toString() + ".race").equalsIgnoreCase("elf");
    }
    public boolean isHuman(Player player)
    {
        return Alpha.plugin.cm.getRaces().getString(player.getUniqueId().toString() + ".race").equalsIgnoreCase("human");
    }
    public boolean isDwarf(Player player)
    {
        return Alpha.plugin.cm.getRaces().getString(player.getUniqueId().toString() + ".race").equalsIgnoreCase("dwarf");
    }

    public boolean isFairy(Player player)
    {
        return Alpha.plugin.cm.getRaces().getString(player.getUniqueId().toString() + ".race").equalsIgnoreCase("fairy");
    }

    public boolean isWizard(Player player)
    {
        return Alpha.plugin.cm.getRaces().getString(player.getUniqueId().toString() + ".race").equalsIgnoreCase("wizard");
    }

    public boolean isMerman(Player player)
    {
        return Alpha.plugin.cm.getRaces().getString(player.getUniqueId().toString() + ".race").equalsIgnoreCase("Merman");
    }

    public String getRace(Player player)
    {
        return Alpha.plugin.cm.getRaces().getString(player.getUniqueId().toString() + ".race");
    }
}
