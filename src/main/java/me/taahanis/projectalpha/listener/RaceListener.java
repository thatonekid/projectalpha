package me.taahanis.projectalpha.listener;

import me.taahanis.projectalpha.Alpha;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class RaceListener implements Listener {



    public Alpha plugin = Alpha.getPlugin(Alpha.class);

    public final static String GUIName = ChatColor.AQUA + "" + ChatColor.BOLD +  "RACE SELECTION";

    public static Inventory myInventory = (Inventory) Bukkit.createInventory(null, 18, GUIName);
    FileConfiguration race = plugin.cm.getRaces();
    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {

        final Player player = event.getPlayer();
        if (!plugin.rm.hasRace(player))
        {

            final ItemStack elf = new ItemStack(Material.ENCHANTED_BOOK, 1);
            ItemMeta elfMeta = elf.getItemMeta();
            elfMeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Elf");
            elf.setItemMeta(elfMeta);

            final ItemStack dwarf = new ItemStack(Material.EXP_BOTTLE, 1);
            ItemMeta dwarfMeta = dwarf.getItemMeta();
            dwarfMeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Dwarf");
            dwarf.setItemMeta(dwarfMeta);

            final ItemStack wizard = new ItemStack(Material.STICK, 1);
            ItemMeta wizardMeta = wizard.getItemMeta();
            wizardMeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Wizard");
            List<String> loreswizard = new ArrayList<String>();
            loreswizard.add(ChatColor.RED + "- Gets a Magic Wand that can shoot spells");
            loreswizard.add(ChatColor.GREEN + "- Teleport Spell");
            wizardMeta.setLore(loreswizard);
            wizard.setItemMeta(wizardMeta);

            final ItemStack fairy = new ItemStack(Material.ELYTRA, 1);
            ItemMeta fairyMeta = fairy.getItemMeta();
            fairyMeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Fairy");
            List<String> loresfairy = new ArrayList<String>();
            loresfairy.add(ChatColor.RED + "- Can fly around with Elytras");
            fairyMeta.setLore(loresfairy);
            fairy.setItemMeta(fairyMeta);

            final ItemStack mermen = new ItemStack(Material.WATER_LILY, 1);
            ItemMeta mermenMeta = mermen.getItemMeta();
            mermenMeta.setDisplayName(ChatColor.GREEN + "" + ChatColor.BOLD + "Merpeople");
            List<String> loresmermen = new ArrayList<String>();
            loresmermen.add(ChatColor.RED + "- Can breathe in Water");
            loresmermen.add(ChatColor.GREEN + "- Get a Trident");
            mermenMeta.setLore(loresmermen);
            mermen.setItemMeta(mermenMeta);


            new BukkitRunnable() {

                @Override
                public void run() {
                    player.openInventory(myInventory);
                    myInventory.setItem(2, elf);
                    myInventory.setItem(4, dwarf);
                    myInventory.setItem(6, fairy);
                    myInventory.setItem(8, wizard);
                    myInventory.setItem(10, mermen);
                }

            }.runTaskLater(this.plugin, 5);
        }
        }


    @EventHandler
    public void onInventory(InventoryClickEvent event)
    {
        ItemStack is = event.getCurrentItem();
        Player p = (Player) event.getWhoClicked();
        Inventory i = event.getInventory();
        if (i.getName().equals(GUIName))
        {
            if (plugin.rm.hasRace(p))
            {
                event.setCancelled(true);
                p.sendMessage(ChatColor.RED + "You already have a race, what are you trying to do?");
                return;
            }
            if (is.getType().equals(Material.ENCHANTED_BOOK))
            {
                event.setCancelled(true);
                p.sendMessage(ChatColor.RED + "You have been reborn as an Elf!");

                race.createSection(p.getUniqueId().toString());
                race.set(p.getUniqueId().toString() + ".race", "Elf");
                plugin.cm.save();
                p.closeInventory();
                p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100000, 3));

            }
            if (is.getType().equals(Material.EXP_BOTTLE))
        {
            event.setCancelled(true);
            p.sendMessage(ChatColor.RED + "You have been reborn as a Dwarf!");

            race.createSection(p.getUniqueId().toString());
            race.set(p.getUniqueId().toString() + ".race", "Dwarf");
            plugin.cm.save();
            p.closeInventory();
            p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 100000, 3));
        }

            if (is.getType().equals(Material.STICK))
            {
                event.setCancelled(true);
                p.sendMessage(ChatColor.RED + "You have been reborn as a Wizard!");

                race.createSection(p.getUniqueId().toString());
                race.set(p.getUniqueId().toString() + ".race", "Wizard");
                plugin.cm.save();
                p.closeInventory();

            }

            if (is.getType().equals(Material.ELYTRA))
        {
            event.setCancelled(true);
            p.sendMessage(ChatColor.RED + "You have been reborn as a Fairy!");

            race.createSection(p.getUniqueId().toString());
            race.set(p.getUniqueId().toString() + ".race", "Fairy");
            plugin.cm.save();
            p.closeInventory();
        }
            if (is.getType().equals(Material.WATER_LILY))
            {
                event.setCancelled(true);
                p.sendMessage(ChatColor.RED + "You have been reborn as a Merperson (Mermaids, Mermen)!");

                race.createSection(p.getUniqueId().toString());
                race.set(p.getUniqueId().toString() + ".race", "Merman");
                ItemStack helmet = new ItemStack(Material.IRON_HELMET);
                ItemMeta helmetMeta = helmet.getItemMeta();
                helmetMeta.setDisplayName(ChatColor.YELLOW + "Underwater Helmet");
                helmetMeta.addEnchant(Enchantment.OXYGEN, 50, true);
                helmet.setItemMeta(helmetMeta);


                plugin.cm.save();
                p.closeInventory();
            }
            //
        }
        //
    }

    @EventHandler
    public void closeInventory(InventoryCloseEvent event)
    {
        final Player p = (Player) event.getPlayer();
        Inventory i = event.getInventory();
        if (i.getName().equals(GUIName) && !plugin.rm.hasRace(p))
        {
            new BukkitRunnable() {

                @Override
                public void run() {
                    p.openInventory(myInventory);
                }

            }.runTaskLater(this.plugin, 5);

        }
    }
    //
}
