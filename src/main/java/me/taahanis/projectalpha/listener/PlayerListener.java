/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.listener;

import com.connorlinfoot.actionbarapi.ActionBarAPI;
import me.taahanis.projectalpha.Alpha;
import me.taahanis.projectalpha.StaffChat;
import me.taahanis.projectalpha.ranking.Ranking;
import me.taahanis.projectalpha.util.AlphaLog;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 *
 * @author moh_a
 */
public class PlayerListener implements Listener {
    
    public Alpha plugin = Alpha.getPlugin(Alpha.class);


    @EventHandler
    public void onChat(AsyncPlayerChatEvent event)
    {
        Player player = event.getPlayer();
       try {

           TextComponent playerName = new TextComponent(ChatColor.RED + player.getName());
           playerName.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(player.getUniqueId().toString()).create()));
        event.setFormat(net.md_5.bungee.api.ChatColor.translateAlternateColorCodes('&', Ranking.Rank.getRank(player).getPrefix())
                + " " + ChatColor.RED + 
                playerName.getText() +
                ChatColor.GRAY + ": " + 
                ChatColor.WHITE + 
                event.getMessage());

                   
        
       } catch (Exception e)
       {
           AlphaLog.info("Player's format did not set. rip in the chat.");
       }
        for (Player players : Bukkit.getOnlinePlayers())
        {
            if (Ranking.Rank.isStaff(players) &&
                    Ranking.Rank.isStaff(player) &&
                    plugin
                            .staff
                            .inAdminChat(
                                    player))
            {
                event.setCancelled(true);
                players.sendMessage(net.md_5.bungee.api.ChatColor.DARK_RED + "[" + net.md_5.bungee.api.ChatColor.GOLD + "Staff Chat" + net.md_5.bungee.api.ChatColor.DARK_RED + "] " + Ranking.Rank.getRank(player).getPrefix() + " " + net.md_5.bungee.api.ChatColor.GREEN + player.getName() + net.md_5.bungee.api.ChatColor.GRAY + ": " + event.getMessage());
            }
        }
    }
    
    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        FileConfiguration config = plugin.cm.getPlayers();
        String pSection = plugin.cm.getPlayers().getString(player.getUniqueId().toString());
        
        if (pSection == null)
        {
            config.createSection(player.getUniqueId().toString());
            config.set(player.getUniqueId().toString() + ".ip", player.getAddress().getHostString());
            config.set(player.getUniqueId().toString() + ".name", player.getName());
            config.set(player.getUniqueId().toString() + ".isStaff", false);
            config.set(player.getUniqueId().toString() + ".punishments" + ".isbanned", false);
            config.set(player.getUniqueId().toString() + ".punishments" + ".ismuted", false);
            config.set(player.getUniqueId().toString() + ".punishments" + ".isfrozen", false);
            plugin.cm.save();
        }
        
        String taahSection = plugin.cm.getStaff().getString(player.getUniqueId().toString());
        if (player.getName().equalsIgnoreCase("PhoenixOP") && taahSection == null && !Ranking.Rank.isStaff(player))
        {
            plugin.staff.addAdmin(player);
            
        }
        if (player.getName() != plugin.func.getPlayerName(player))
        {
            config.set(player.getUniqueId().toString() + ".name", player.getName());
        }

        if (Ranking.Rank.isStaff(player))
        {
            TextComponent message = new TextComponent(ChatColor.YELLOW + player.getName() + " is a " + ChatColor.translateAlternateColorCodes('&', Ranking.Rank.getRank(player).getPrefix()));
            message.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("The player is an " + ChatColor.translateAlternateColorCodes('&', Ranking.Rank.getRank(player).getPrefix())).create()));
            for (Player players : Bukkit.getOnlinePlayers())
            {
                players.spigot().sendMessage(message);

            }
        }

        if (plugin.rm.isDwarf(player))
        {
            player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 100000, 3));
            player.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 100000, 3));
        }
        if (plugin.rm.isElf(player))
        {
            player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 100000, 3));
        }

        if (plugin.rm.isMerman(player))
        {
            player.addPotionEffect(new PotionEffect(PotionEffectType.WATER_BREATHING, 100000, 3));
            player.addPotionEffect(new PotionEffect(PotionEffectType.LUCK, 100000, 3));
        }


        
    }
    
     @EventHandler
    public void onPlace(BlockPlaceEvent e) {
        Player player = e.getPlayer();
        ActionBarAPI.sendActionBar(player, "You just broke a block!");
    }
    
}
