/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package me.taahanis.projectalpha.listener;

import me.taahanis.projectalpha.Alpha;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;

/**
 *
 * @author moh_a
 */
public class ScoreboardListener implements Listener {
    
    
    ScoreboardManager manager = Bukkit.getScoreboardManager();

    Scoreboard board = manager.getNewScoreboard();
 
    Objective objective = board.registerNewObjective("showhealth", "health");
    
    @EventHandler
    public void onJoin(PlayerJoinEvent event)
    {
        
        
        objective.setDisplaySlot(DisplaySlot.BELOW_NAME);
        objective.setDisplayName(ChatColor.RED + "Health");
        Player p = event.getPlayer();
        p.setScoreboard(board);
        p.setHealth(p.getHealth());
        for (Player online : Bukkit.getOnlinePlayers())
        {
            
            p.setScoreboard(board);
        }
    }
    
    @EventHandler
    public void onLeave(PlayerQuitEvent event)
    {
        OfflinePlayer p = event.getPlayer();
        board.clearSlot(DisplaySlot.BELOW_NAME);
        
    }
    
}
