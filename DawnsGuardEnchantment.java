package com.projectAlpha;




import java.util.HashMap;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Husk;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Stray;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Wither;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;
import net.minecraft.server.v1_12_R1.InventoryClickType;

public class DawnsGuardEnchantment extends Enchantment implements Listener{
	
	//The code below for handling armor equip checking comes from this source https://github.com/Eniripsa96/MCCore/blob/master/src/com/rit/sucy/event/EquipListener.java
	/*
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		ArmorEquipChecker.checkArmorEquips(event.getWhoClicked());
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event) {
		ArmorEquipChecker.checkArmorEquips(event.getPlayer());
	}
	
	@EventHandler
	public void onEntityDamageEntity(EntityDamageByEntityEvent event) {
		if(event.getDamager() instanceof Zombie || event.getDamager() instanceof Skeleton || event.getDamager() instanceof WitherSkeleton ||
		   event.getDamager() instanceof Wither || event.getDamager() instanceof PigZombie || event.getDamager() instanceof Husk ||
		   event.getDamager() instanceof Stray) {
			if(event.getEntity() instanceof CraftPlayer) {
				Player player = (Player) event.getEntity();
				int resistance = ArmorEquipChecker.checkArmorEquips(player);
				//System.out.println(player.getHealth());
				if(ArmorEquipChecker.blessedList.containsKey(player.getName())) {
					
					//System.out.println("before: " + event.getDamage());
					event.setDamage(event.getDamage() - resistance);
					//System.out.println("after: " + event.getDamage());
				}
			}
		}
	}
	*/
	public DawnsGuardEnchantment(int id) {
		super(id);
	}
	
	
	@Override
	public int getId() {
		return 108;
	}
	
	@Override
	public boolean canEnchantItem(ItemStack item) {
		if(item.getType() == Material.LEATHER_CHESTPLATE || item.getType() == Material.GOLD_CHESTPLATE || item.getType() == Material.IRON_CHESTPLATE ||
		   item.getType() == Material.DIAMOND_CHESTPLATE) {
			return true;
		}
		return false;
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return null;
	}

	@Override
	public int getMaxLevel() {
		return 1;
	}

	@Override
	public String getName() {
		return "Dawn's Guard";
	}

	@Override
	public int getStartLevel() {
		return 1;
	}

	@Override
	public boolean isCursed() {
		return false;
	}

	@Override
	public boolean isTreasure() {
		return false;
	}

}
