package com.projectAlpha;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentOffer;
import org.bukkit.entity.Husk;
import org.bukkit.entity.PigZombie;
import org.bukkit.entity.Player;
import org.bukkit.entity.Skeleton;
import org.bukkit.entity.Stray;
import org.bukkit.entity.Villager;
import org.bukkit.entity.Wither;
import org.bukkit.entity.WitherSkeleton;
import org.bukkit.entity.Zombie;
import org.bukkit.event.Event;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.enchantment.EnchantItemEvent;
import org.bukkit.event.enchantment.PrepareItemEnchantEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;

import net.md_5.bungee.api.ChatColor;


public class CustomEnchants extends JavaPlugin implements Listener{
	
	public static JavaPlugin plugin;
	private int BasicDamage = 0;
	private int EntityDamage = 0;
	
	public LifesteelEnchant lifesteel = new LifesteelEnchant(101);
	//public SoulboundEnchant soulbound = new SoulboundEnchant(102);
	public MidasTouchEnchantment midasTouch = new MidasTouchEnchantment(103);
	public WrathOfZeusEnchantment wrathOfZeus = new WrathOfZeusEnchantment(104);
	public JudgementEnchantment judgement = new JudgementEnchantment(105);
	public BlessedEnchantment blessed = new BlessedEnchantment(106);
	public RetributionEnchantment retribution = new RetributionEnchantment(107);
	public DawnsGuardEnchantment dawnsGuard = new DawnsGuardEnchantment(108);
	public CurseOfBurningEnchantment burning = new CurseOfBurningEnchantment(109);
	
	
	public static HashMap<String, String> killerVictimList = new HashMap<>();
	public static HashMap<String, Integer> kills = new HashMap<>();
	
	@Override
	public void onEnable() {
		plugin = this;
		loadEnchantments();
		this.getServer().getPluginManager().registerEvents(this, this);
		this.getServer().getPluginManager().registerEvents(lifesteel, this);
		//this.getServer().getPluginManager().registerEvents(soulbound, this);
		this.getServer().getPluginManager().registerEvents(midasTouch, this);
		this.getServer().getPluginManager().registerEvents(wrathOfZeus, this);
		this.getServer().getPluginManager().registerEvents(judgement, this);
		this.getServer().getPluginManager().registerEvents(blessed, this);
		this.getServer().getPluginManager().registerEvents(retribution, this);
		this.getServer().getPluginManager().registerEvents(dawnsGuard, this);
		this.getServer().getPluginManager().registerEvents(burning, this);
		
		
		//remove this
		
	}
	
	//In order to properly calculate the final damage when taking into account all the enchantments,
	//two events must be trigged separately and one must execute before the other because the second one depends on the result of
	//the first. The two must be separate because if they are not, specific entity attack entity functions will not work for
	//a regular damage event and if the first is added to the second, the event will only trigger for entity attack entity events not
	//and not the basic general damage events.
	
	
	@EventHandler(priority = EventPriority.NORMAL)
	public void onEntityDamage(EntityDamageEvent event) {
		//execute this method BEFORE the below method
		//System.out.println("1: " + event.getFinalDamage());
		BasicDamage = calculateDamage(event);
		if(event.getFinalDamage() + BasicDamage <= 0) {
			event.setDamage(0);
		} else {
			event.setDamage(event.getFinalDamage() + BasicDamage);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGH)
	public void onEntityDamageEntity(EntityDamageByEntityEvent event) {
		//once the final damage is calculated we can determine whether the player died and if so from what.
		//System.out.println("2: " + event.getFinalDamage());
		//System.out.println("second");
		//System.out.println("---");
		EntityDamage = calculateDamage(event);
		//System.out.println(EntityDamage);
		//System.out.println(BasicDamage);
		//System.out.println(event.getDamage());
		
		
		//if new calculated final total damge is valid ( >= 0)
		if(event.getFinalDamage() + EntityDamage <= 0) {
			event.setDamage(0);
		} else {
			event.setDamage(event.getFinalDamage() + EntityDamage);
			System.out.println(event.getFinalDamage());
		}
	}
	
	public int calculateDamage(EntityDamageEvent event) {
		//handle non entity attack entity damage and resistance here
		int bonusResistance = 0;
		int bonusDamage = 0;
		
		if(event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			bonusResistance += ArmorEquipChecker.checkArmorEquipsDawn(player);
			System.out.println(bonusResistance);
			bonusDamage = bonusDamage - bonusResistance;
		}
		return bonusDamage;
	}
	
	public int calculateDamage(EntityDamageByEntityEvent event) {
		
		//handle entity attack entity damage and resistance here
		
		int bonusResistance = 0;
		int bonusDamage = 0;
		
		boolean judged = false;
		//handle blessed resistance bonus	
		
		
		
		if(event.getDamager() instanceof Zombie || event.getDamager() instanceof Skeleton || event.getDamager() instanceof WitherSkeleton ||
				
			event.getDamager() instanceof Wither || event.getDamager() instanceof PigZombie || event.getDamager() instanceof Husk ||
			event.getDamager() instanceof Stray) {
			if(event.getEntity() instanceof CraftPlayer) {
				Player player = (Player) event.getEntity();
				int resistance = ArmorEquipChecker.checkArmorEquipsBlessed(player);
				//System.out.println(player.getHealth());
				if(ArmorEquipChecker.blessedList.containsKey(player.getName())) {
					
					//System.out.println("before: " + event.getDamage());
					bonusResistance += resistance;
					//System.out.println("after: " + event.getDamage());
				}
			}
		}
		
		//handle Retribution damage bonus
		
		if(event.getEntity() instanceof Player && event.getDamager() instanceof Player) {
			Player victim = (Player) event.getEntity();
			Player damager = (Player) event.getDamager();
			
			int additionalDamage = 0;
			
			if(killerVictimList.get(damager.getName()) != null && killerVictimList.get(damager.getName()).equals(victim.getName())) {
				ItemStack item = damager.getInventory().getItemInMainHand();
				Map<Enchantment, Integer> enchants = item.getEnchantments();
				if(enchants != null) {
					
					for(Enchantment enchant: enchants.keySet()) {
						if(enchant.getName().equals(retribution.getName())) {
							additionalDamage += 5;
						}
					}
				}
			}
			bonusDamage += additionalDamage;
		}
		
		//handle Judgement damage bonus
		
		if(event.getDamager() instanceof Player && (event.getEntity() instanceof Player || event.getEntity() instanceof Villager)) {
			Villager v;
			Player damaged;
			boolean killed = false;
			if(event.getEntity() instanceof Villager) {
				v = (Villager) event.getEntity();
				if(event.getDamage() >= v.getHealth()) {
					killed = true;
				}
			} else if(event.getEntity() instanceof Player) {
				damaged = (Player) event.getEntity();
				if(event.getDamage() >= damaged.getHealth()) {
					killed = true;
				}
			}
			
			if(killed) {
				if(kills.containsKey(event.getDamager().getName())) {
					kills.replace(event.getDamager().getName(), kills.get(event.getDamager().getName()) + 1);
				} else {
					kills.put(event.getDamager().getName(), 1);
				}
				event.getDamager().sendMessage(ChatColor.RED + "Judgement is coming.");
			}
		}
		
		if(event.getDamager() instanceof Player) {
			Player player = (Player) event.getDamager();
			
			ItemStack item = player.getInventory().getItemInMainHand();
			Map<Enchantment, Integer> enchants = item.getEnchantments();
			if(enchants != null) {
				
				for(Enchantment enchant: enchants.keySet()) {
					
					
					if(enchant.getName().equalsIgnoreCase(judgement.getName())) {
						Integer extraDamage = kills.get(event.getEntity().getName());
						if(extraDamage != null) {
							bonusDamage += extraDamage;
						}//player.getInventory().remove(item);
					}
				}
			}
		}
		
		
		
		
		//handle lifesteal (this may have to be moved to a separate handler)
		if(event.getDamage() > 1) {
			if(event.getDamager() instanceof Player) {
				Player player = (Player) event.getDamager();
				ItemStack item = player.getInventory().getItemInMainHand();
				Map<Enchantment, Integer> enchants = item.getEnchantments();
				for(Enchantment enchant: enchants.keySet()) {
					
					
					if(enchant.getName().equalsIgnoreCase("Lifesteal")) {
						player.setHealth(player.getHealth() + 1);
						//player.getInventory().remove(item);
					}
				}
			}
		}
		
		
		//calculate final damage for entity attack entity events
		 bonusDamage = bonusDamage - bonusResistance;
		
		 
		//handle removing Players from retribution list
		if(event.getDamager() instanceof Player && event.getEntity() instanceof Player) {
			Player damager = (Player) event.getDamager();
			Player victim = (Player) event.getEntity();
			if(event.getFinalDamage() >= victim.getHealth()) {
				killerVictimList.put(damager.getName(), victim.getName());
				if(killerVictimList.containsKey(victim.getName()) &&  killerVictimList.get(victim.getName()).equals(damager)) {
					killerVictimList.remove(victim.getName());
				}
			}
		}
		//System.out.println(bonusDamage);
		return bonusDamage;
	}
	
	@EventHandler
	public void onItemPlaceEnchantmentTable(PrepareItemEnchantEvent event) {
		//event.setCancelled(false);
		//since custom enchants cannot be added to the enchantment table, add a custom enchant anyway to erase the preview text
		event.getOffers()[0].setEnchantment(lifesteel);
		event.getOffers()[1].setEnchantment(lifesteel);
		event.getOffers()[2].setEnchantment(lifesteel);
	}
	
	@EventHandler
	public void onEnchant(EnchantItemEvent event) {
		event.setCancelled(true);
		event.getItem().addUnsafeEnchantment(lifesteel, 2);
		ItemMeta meta = event.getItem().getItemMeta();
		List<String> lore = meta.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(ChatColor.GRAY + lifesteel.getName() + " II");
		meta.setLore(lore);
		event.getItem().setItemMeta(meta);
	}
	
	
	
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		
		
		
		Player player = event.getPlayer();
		
		ItemStack item = new ItemStack(Material.DIAMOND_SWORD);
		item.addUnsafeEnchantment(lifesteel, 1);
		ItemMeta meta = item.getItemMeta();
		List<String> lore = meta.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(ChatColor.GRAY + lifesteel.getName() + " I");
		meta.setLore(lore);
		item.setItemMeta(meta);
		
		
		/*item.addUnsafeEnchantment(soulbound, 1);
		meta = item.getItemMeta();
		lore = meta.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(ChatColor.GRAY + soulbound.getName() + " I");
		meta.setLore(lore);
		item.setItemMeta(meta);
		*/

		item.addUnsafeEnchantment(judgement, 1);
		meta = item.getItemMeta();
		lore = meta.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(ChatColor.GRAY + judgement.getName() + " I");
		meta.setLore(lore);
		item.setItemMeta(meta);
		player.getInventory().addItem(item);
		
		
		ItemStack item2 = new ItemStack(Material.BOW);
		item2.addUnsafeEnchantment(wrathOfZeus, 1);
		ItemMeta meta2 = item2.getItemMeta();
		lore = meta2.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(ChatColor.GRAY + wrathOfZeus.getName() + " I");
		meta2.setLore(lore);
		item2.setItemMeta(meta2);
		player.getInventory().addItem(item2);
		
		item = new ItemStack(Material.GOLD_PICKAXE);
		item.addUnsafeEnchantment(midasTouch, 1);
		meta = item.getItemMeta();
		lore = meta.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(ChatColor.GRAY + midasTouch.getName() + " I");
		meta.setLore(lore);
		item.setItemMeta(meta);
		player.getInventory().addItem(item);
		
		item = new ItemStack(Material.LEATHER_HELMET);
		item.addUnsafeEnchantment(dawnsGuard, 1);
		meta = item.getItemMeta();
		lore = meta.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(org.bukkit.ChatColor.GRAY + dawnsGuard.getName() + " I");
		meta.setLore(lore);
		item.setItemMeta(meta);
		player.getInventory().addItem(item);
		
		item = new ItemStack(Material.LEATHER_CHESTPLATE);
		item.addUnsafeEnchantment(dawnsGuard, 1);
		meta = item.getItemMeta();
		lore = meta.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(org.bukkit.ChatColor.GRAY + dawnsGuard.getName() + " I");
		meta.setLore(lore);
		item.setItemMeta(meta);
		player.getInventory().addItem(item);
		
		item = new ItemStack(Material.LEATHER_LEGGINGS);
		item.addUnsafeEnchantment(dawnsGuard, 1);
		meta = item.getItemMeta();
		lore = meta.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(org.bukkit.ChatColor.GRAY + dawnsGuard.getName() + " I");
		meta.setLore(lore);
		item.setItemMeta(meta);
		player.getInventory().addItem(item);
		
		item = new ItemStack(Material.LEATHER_BOOTS);
		item.addUnsafeEnchantment(dawnsGuard, 1);
		meta = item.getItemMeta();
		lore = meta.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(org.bukkit.ChatColor.GRAY + dawnsGuard.getName() + " I");
		meta.setLore(lore);
		item.setItemMeta(meta);
		player.getInventory().addItem(item);
		
		ItemStack item3 = new ItemStack(Material.DIAMOND_SWORD);
		item3.addUnsafeEnchantment(retribution, 1);
		meta = item3.getItemMeta();
		lore = meta.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(org.bukkit.ChatColor.GRAY + retribution.getName() + " I");
		meta.setLore(lore);
		item3.setItemMeta(meta);
		player.getInventory().addItem(item3);
		
		
		ItemStack item4 = new ItemStack(Material.DIAMOND_SWORD) ;
		item4.addUnsafeEnchantment(burning, 1);
		meta = item4.getItemMeta();
		lore = meta.getLore();
		if(lore == null) {
			lore = new ArrayList<>();
		}
		lore.add(ChatColor.RED + burning.getName());
		meta.setLore(lore);
		item4.setItemMeta(meta);
		player.getInventory().addItem(item4);
		
	}
	
	@SuppressWarnings("unchecked")
	public void onDisable() {
		try {
			Field byIdField = Enchantment.class.getDeclaredField("byId");
			Field byNameField = Enchantment.class.getDeclaredField("byName");
			
			byIdField.setAccessible(true);
			byNameField.setAccessible(true);
			
			HashMap<Integer, Enchantment> byId = (HashMap<Integer, Enchantment>) byIdField.get(null);
			HashMap<Integer, Enchantment> byName = (HashMap<Integer, Enchantment>) byNameField.get(null);
			
			if(byId.containsKey(lifesteel.getId())) {
				byId.remove(lifesteel.getId());
			}
			
			if(byName.containsKey(lifesteel.getName())) {
				byName.remove(lifesteel.getName());
			}
			/*
			if(byId.containsKey(soulbound.getId())) {
				byId.remove(soulbound.getId());
			}
			
			if(byName.containsKey(soulbound.getName())) {
				byName.remove(soulbound.getName());
			}
			*/
			if(byId.containsKey(midasTouch.getId())) {
				byId.remove(midasTouch.getId());
			}
			
			if(byName.containsKey(midasTouch.getName())) {
				byName.remove(midasTouch.getName());
			}
			
			if(byId.containsKey(wrathOfZeus.getId())) {
				byId.remove(wrathOfZeus.getId());
			}
			
			if(byName.containsKey(wrathOfZeus.getName())) {
				byName.remove(wrathOfZeus.getName());
			}
			
			if(byId.containsKey(judgement.getId())) {
				byId.remove(judgement.getId());
			}
			
			if(byName.containsKey(judgement.getName())) {
				byName.remove(judgement.getName());
			}
			
			if(byId.containsKey(blessed.getId())) {
				byId.remove(blessed.getId());
			}
			
			if(byName.containsKey(blessed.getName())) {
				byName.remove(blessed.getName());
			}
			
		} catch(Exception ignored) {
			
		}
	}
	
	private void loadEnchantments() {
		try {
			try {
				Field f = Enchantment.class.getDeclaredField("acceptingNew");
				f.setAccessible(true);
				f.set(null, true);
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			registerEnchant(lifesteel);
			//registerEnchant(soulbound);
			registerEnchant(midasTouch);
			registerEnchant(wrathOfZeus);
			registerEnchant(judgement);
			registerEnchant(blessed);
			registerEnchant(retribution);
			registerEnchant(dawnsGuard);
			registerEnchant(burning);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	private void registerEnchant(Enchantment ench) {
		try {
			Enchantment.registerEnchantment(ench);
		} catch(IllegalArgumentException e) {
			e.printStackTrace();
		}
	}
}
