package com.projectAlpha;

public class SoulboundEnchant {
	
}

/*
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;

import org.apache.logging.log4j.core.appender.rolling.OnStartupTriggeringPolicy;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class SoulboundEnchant extends Enchantment implements Listener{
	
	
	public static ArrayList<SoulboundItem> soulboundItems = new ArrayList<>();
	
	public SoulboundEnchant(int id) {
		super(id);
	}
	
	@EventHandler
	public void onPlayerSpawn(PlayerRespawnEvent event) {
		//System.out.println("onPlayerSpawn called");
		//System.out.println(soulboundItems.size());
		Player player = event.getPlayer();
		
		
		ListIterator<SoulboundItem> iter = soulboundItems.listIterator();
		while(iter.hasNext()){
			SoulboundItem item = iter.next();
			if(item.getPlayerName().equals(player.getName())) {
				player.getInventory().addItem(item.getItemStack());
			}
			iter.remove();
		}
		
	}
	
	@EventHandler
	public void onPlayerDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			int resistance = ArmorEquipChecker.(player);
			if((player.getHealth() + resistance) - event.getDamage() <= 0) {
				for (ItemStack item : player.getInventory()) {
					boolean shouldDrop = true;
					if(item != null) {
						Map<Enchantment, Integer> enchants = item.getEnchantments();
						if(enchants != null) {
							for(Enchantment enchant: enchants.keySet()) {
								if(enchant.getName().equals(this.getName())) {
									soulboundItems.add(new SoulboundItem(player, item));
									shouldDrop = false;
									//player.getInventory().remove(item);
								}
							}
						}
						if(item.getType() != Material.AIR && shouldDrop) {
							player.getWorld().dropItemNaturally(player.getLocation(), item);
						}
						
					}
					
					
					
				}
				player.getInventory().clear();
			}
		}
	}
	
	@Override
	public int getId() {
		return 102;
	}
	
	@Override
	public boolean canEnchantItem(ItemStack item) {
		if(item.getType() == Material.WOOD_SWORD || item.getType() == Material.GOLD_SWORD || item.getType() == Material.IRON_SWORD
		|| item.getType() == Material.DIAMOND_SWORD) {
			return true;
		}
		return false;
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return null;
	}

	@Override
	public int getMaxLevel() {
		return 1;
	}

	@Override
	public String getName() {
		return "Soulbound";
	}

	@Override
	public int getStartLevel() {
		return 1;
	}

	@Override
	public boolean isCursed() {
		return false;
	}

	@Override
	public boolean isTreasure() {
		return false;
	}

}*/
