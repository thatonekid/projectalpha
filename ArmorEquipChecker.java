package com.projectAlpha;

import java.util.HashMap;
import java.util.Map;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ArmorEquipChecker {
	
	public static HashMap<String, Integer> blessedList = new HashMap<>();
	public static HashMap<String, Integer> dawnsGuardList = new HashMap<>();
	
	public static int checkArmorEquipsBlessed(HumanEntity player) {
		player = (Player) player;
		ItemStack[] equips = player.getEquipment().getArmorContents();
		int resistance = 0;
		if(equips != null) {
			
			for(ItemStack item: equips) {
				
				if(item != null) {
					Map<Enchantment, Integer> enchants = item.getEnchantments();
					
					if(enchants != null) {
						for(Enchantment enchant: enchants.keySet()) {
							
							//System.out.println(enchant.getName());
							if(enchant.getName().equalsIgnoreCase("Blessed")) {
								resistance++;
								
								//player.getInventory().remove(item);
							}
							
							
							
							
						}
					}
				}
			}
			if(blessedList.containsKey(player.getName()) && blessedList.get(player.getName()) != resistance) {
				blessedList.replace(player.getName(), resistance);
				player.sendMessage("1");
			} else if(!blessedList.containsKey(player.getName())) {
				blessedList.put(player.getName(), resistance);
			}
			
			
			
			//player.sendMessage("" + resistance);
		}
		
		
		
		return resistance;
	}	
	
	
	public static int checkArmorEquipsDawn(HumanEntity player) {
		
		if(!isDay(player)) {
			return 0;
		}
		player = (Player) player;
		ItemStack[] equips = player.getEquipment().getArmorContents();
		int resistance = 0;
		if(equips != null) {
			
			for(ItemStack item: equips) {
				
				if(item != null) {
					Map<Enchantment, Integer> enchants = item.getEnchantments();
					
					if(enchants != null) {
						for(Enchantment enchant: enchants.keySet()) {
							
							//System.out.println(enchant.getName());
							if(enchant.getName().equalsIgnoreCase("Dawn's Guard")) {
								resistance++;
								
								//player.getInventory().remove(item);
							}
							
							
							
							
						}
					}
				}
			}
			if(dawnsGuardList.containsKey(player.getName()) && dawnsGuardList.get(player.getName()) != resistance) {
				dawnsGuardList.replace(player.getName(), resistance);
			} else if(!dawnsGuardList.containsKey(player.getName())) {
				dawnsGuardList.put(player.getName(), resistance);
			}
			
			
			
			//player.sendMessage("" + resistance);
		}
		
		
		
		return resistance;
	}
	
	
	
	
	public static boolean isDay(HumanEntity player) {
	    
	    long time = player.getWorld().getTime();

	    if(time > 0 && time < 12300) {
	        return true;
	    } else {
	        return false;
	    }
	}
	
}
