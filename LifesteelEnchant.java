package com.projectAlpha;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class LifesteelEnchant extends Enchantment implements Listener{
	
	
	
	public LifesteelEnchant(int id) {
		super(id);
	}
	
	
	@Override
	public int getId() {
		return 101;
	}
	
	@Override
	public boolean canEnchantItem(ItemStack item) {
		if(item.getType() == Material.WOOD_SWORD || item.getType() == Material.GOLD_SWORD || item.getType() == Material.IRON_SWORD
		|| item.getType() == Material.DIAMOND_SWORD) {
			return true;
		}
		return false;
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return null;
	}

	@Override
	public int getMaxLevel() {
		return 2;
	}

	@Override
	public String getName() {
		return "Lifesteal";
	}

	@Override
	public int getStartLevel() {
		return 1;
	}

	@Override
	public boolean isCursed() {
		return false;
	}

	@Override
	public boolean isTreasure() {
		return false;
	}

}
