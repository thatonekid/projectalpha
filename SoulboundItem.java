package com.projectAlpha;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class SoulboundItem {
	private String playerName;
	private ItemStack item;
	
	public SoulboundItem(Player player, ItemStack item) {
		System.out.println("created new soulbound item");
		this.playerName = player.getName();
		this.item = item;
	}
	
	public String getPlayerName() {
		return this.playerName;
	}
	
	public ItemStack getItemStack() {
		return this.item;
	}
}
