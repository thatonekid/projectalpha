package com.projectAlpha;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import org.apache.commons.io.output.ThresholdingOutputStream;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class WrathOfZeusEnchantment extends Enchantment implements Listener{
	
	private static ArrayList<Entity> projectiles = new ArrayList<>();
	
	
	@EventHandler
	public void onProjectileHit(ProjectileHitEvent event) {
		
		try {
			ListIterator<Entity> iter = projectiles.listIterator();
			while(iter.hasNext()){
				Entity id = iter.next();
				if(id.equals(event.getEntity())) {
					event.getEntity().getWorld().strikeLightning(event.getEntity().getLocation());
					iter.remove();
					
				}
				/*
				if((id.equals(event.getEntity()))) {
					System.out.println("dead");
					iter.remove();
				}*/
				
			}
		} catch(Exception e) {
			projectiles = new ArrayList<>();
		}
		
	}
	
	@EventHandler
	public void onArrowLand(EntityShootBowEvent event) {
		
		if(event.getEntity() instanceof Player) {
			Player player = (Player) event.getEntity();
			
			ItemStack item = player.getInventory().getItemInMainHand();
			Map<Enchantment, Integer> enchants = item.getEnchantments();
			if(enchants != null) {
				
				for(Enchantment enchant: enchants.keySet()) {
					
					System.out.println(this.getName());
					System.out.println(enchant.getName());
					if(enchant.getName().equalsIgnoreCase(this.getName())) {
						projectiles.add(event.getProjectile());
						//player.getInventory().remove(item);
					}
				}
			}
		}
		
		
	}
	
	
	public WrathOfZeusEnchantment(int id) {
		super(id);
	}
	
	
	@Override
	public int getId() {
		return 104;
	}
	
	@Override
	public boolean canEnchantItem(ItemStack item) {
		if(item.getType() == Material.BOW) {
			return true;
		}
		return false;
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return null;
	}

	@Override
	public int getMaxLevel() {
		return 2;
	}

	@Override
	public String getName() {
		return "Wrath Of Zeus";
	}

	@Override
	public int getStartLevel() {
		return 1;
	}

	@Override
	public boolean isCursed() {
		return false;
	}

	@Override
	public boolean isTreasure() {
		return false;
	}

}
