package com.projectAlpha;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.md_5.bungee.api.ChatColor;

public class MidasTouchEnchantment extends Enchantment implements Listener{
	
	
	
	public MidasTouchEnchantment(int id) {
		super(id);
	}
	
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		ItemStack item = event.getPlayer().getInventory().getItemInMainHand();
		Map<Enchantment, Integer> enchants = item.getEnchantments();
		if(enchants != null) {
			for(Enchantment enchant: enchants.keySet()) {
				if(enchant.getName().equals(this.getName())) {
					event.setCancelled(true);
					event.getBlock().setType(Material.AIR);
					Location location = event.getBlock().getLocation();
					location.setX(location.getX() + .5);
					location.setY(location.getY() + .5);
					location.setZ(location.getZ() + .5);
					event.getBlock().getWorld().dropItemNaturally(location, new ItemStack(Material.GOLD_NUGGET));
					item.setDurability( (short)(item.getDurability() + 1) );
					if(item.getDurability() == 33) {
						event.getPlayer().getInventory().remove(item);
					}
					//player.getInventory().remove(item);
				}
			}
		}
	}
	
	@Override
	public int getId() {
		return 103;
	}
	
	@Override
	public boolean canEnchantItem(ItemStack item) {
		if(item.getType() == Material.GOLD_PICKAXE) {
			return true;
		}
		return false;
	}

	@Override
	public boolean conflictsWith(Enchantment arg0) {
		return false;
	}

	@Override
	public EnchantmentTarget getItemTarget() {
		return null;
	}

	@Override
	public int getMaxLevel() {
		return 3;
	}

	@Override
	public String getName() {
		return "Midas Touch";
	}

	@Override
	public int getStartLevel() {
		return 1;
	}

	@Override
	public boolean isCursed() {
		return false;
	}

	@Override
	public boolean isTreasure() {
		return false;
	}

}
